#!/bin/bash

# root login check
# Prompt the user using whiptail
if whiptail --title "Root Privileges Required" --yesno "Do you want to switch to the ROOT user?" 8 78; then
    echo "User agreed to switch to root user."
    # Check if the script is not run as root
    if [ "$EUID" -ne 0 ]; then
        echo "This script requires root privileges."
        echo "Switching to root user..."
        su - || {
            echo "Failed to switch to root user. Exiting."
            rm -rf /tmp/freepbx_debian_install.sh
            exit 1
        }
    else
        echo "Already running as root."
    fi
else
    echo "User chose not to switch to root user. Exiting."
    rm -rf /tmp/freepbx_debian_install.sh
    exit 1
fi

# Your script logic here for when root login is successful
echo "Root login succeeded."

sleep 1
####System Update
update_system() {
    echo "Updating package lists..."
    apt-get update -y -qq

    echo "Upgrading packages..."
    apt-get upgrade -y -qq

    echo "Performing distribution upgrade..."
    apt-get dist-upgrade -y -qq

    echo "Removing unnecessary packages..."
    apt-get autoremove -y -qq

    echo "Cleaning up..."
    apt-get clean

    echo "System update completed."
}

# Automatically start the update process
echo "Starting system update..."
sleep 1  # Add a short delay to allow the message to be seen
update_system

pckarr1=(build-essential git curl wget libnewt-dev libssl-dev libncurses5-dev subversion libsqlite3-dev libjansson-dev libxml2-dev uuid-dev default-libmysqlclient-dev htop sngrep lame ffmpeg mpg123 git vim curl wget libnewt-dev libssl-dev libncurses5-dev subversion libsqlite3-dev build-essential libjansson-dev libxml2-dev uuid-dev expect build-essential openssh-server apache2 mariadb-server mariadb-client bison flex curl sox libncurses5-dev libssl-dev mpg123 libxml2-dev libnewt-dev libsqlite3-dev pkg-config automake libtool autoconf git unixodbc-dev uuid uuid-dev libasound2-dev libogg-dev libvorbis-dev libicu-dev libcurl4-openssl-dev odbc-mariadb libical-dev libneon27-dev libsrtp2-dev libspandsp-dev sudo subversion libtool-bin python-dev-is-python3 unixodbc vim wget libjansson-dev software-properties-common nodejs npm ipset iptables fail2ban php-soap sqlite3)
for i in  ${pckarr1[*]}
 do
  isinstalled=$(dpkg -s $i 2>&1 | head -n 1)
  if [ !  "$isinstalled" == "dpkg-query: package '$i' is not installed and no information is available" ];
   then
    echo '#### Dependencies Packages ####'
    echo Package  $i already installed - Successful.
  else
    echo $i is not INSTALLED!!!! 
    apt install -y -qq $i 
    echo Package  $i  installed now - Successful.
  fi
  done

systemctl start mariadb
systemctl enable mariadb

sleep 5

pckarr1=(php8.2 php8.2-curl php8.2-cli php8.2-mysql php8.2-mbstring php8.2-gd php8.2-xml php8.2-intl php8.2-redis php8.2-bz2 php8.2-ldap php8.2-bcmath php8.2-sqlite3 php-pear libapache2-mod-php)
for i in  ${pckarr1[*]}
 do
  isinstalled=$(dpkg -s $i 2>&1 | head -n 1)
  if [ !  "$isinstalled" == "dpkg-query: package '$i' is not installed and no information is available" ];
   then
    echo '#### Dependencies Packages ####'
    echo Package  $i already installed - Successful.
  else
    echo $i is not INSTALLED!!!! 
    apt install -y -qq $i 
    echo Package  $i  installed now - Successful.
  fi
  done

systemctl start mariadb
systemctl enable mariadb

sleep 5


timedatectl set-timezone Asia/Colombo

############################Download asterisk source and compilation
cd /usr/src/
rm -rf  /usr/src/asterisk-20*
wget http://downloads.asterisk.org/pub/telephony/asterisk/asterisk-20-current.tar.gz
tar -xf asterisk-20-current.tar.gz
rm -rf asterisk-20-current.tar.gz
pushd /usr/src/asterisk-20*
./contrib/scripts/get_mp3_source.sh 
./contrib/scripts/install_prereq install
make distclean
./configure  --libdir=/usr/lib64 --with-pjproject-bundled --with-jansson-bundled
make menuselect.makeopts
menuselect/menuselect --enable format_mp3 --enable res_config_mysql --enable app_macro  
make
make install
make samples
make config
ldconfig

#############################Create asterisk user and give permission
groupadd asterisk
useradd -r -d /var/lib/asterisk -g asterisk asterisk
usermod -aG audio,dialout asterisk
chown -R asterisk:asterisk /etc/asterisk
chown -R asterisk:asterisk /var/{lib,log,spool}/asterisk
chown -R asterisk:asterisk /usr/lib64/
sed -i 's|;runuser|runuser|' /etc/asterisk/asterisk.conf
sed -i 's|;rungroup|rungroup|' /etc/asterisk/asterisk.conf
echo "/usr/lib64" >> /etc/ld.so.conf.d/x86_64-linux-gnu.conf
ldconfig
popd


#######################Configure Apache web server
sed -i 's/\(^upload_max_filesize = \).*/\512M/' /etc/php/8.2/apache2/php.ini
sed -i 's/\(^memory_limit = \).*/\1256M/' /etc/php/8.2/apache2/php.ini
sed -i 's/^\(User\|Group\).*/\1 asterisk/' /etc/apache2/apache2.conf
sed -i 's/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf
a2enmod rewrite
systemctl restart apache2
rm /var/www/html/index.html

############ Configure ODBC
cat > /etc/odbcinst.ini << EOF
[MySQL]
Description = ODBC for MariaDB
Driver = libmaodbc.so
FileUsage = 1
EOF

# Cut/paste the following lines into a Linux CLI
cat > /etc/odbc.ini << EOF
[MySQL-asteriskcdrdb]
Description = MariaDB connection to 'asteriskcdrdb' database
driver = MySQL
server = localhost
database = asteriskcdrdb
Port = 3306
Socket = /var/run/mysqld/mysqld.sock
option = 3
EOF

########################Install FreePBX
cd /usr/src
wget http://mirror.freepbx.org/modules/packages/freepbx/freepbx-17.0-latest-EDGE.tgz
tar zxvf freepbx-17.0-latest-EDGE.tgz
cd /usr/src/freepbx
./start_asterisk start
./install -n

fwconsole ma installall
fwconsole chown
fwconsole restart

# Cut/paste the following lines into a Linux CLI
cat > /etc/systemd/system/freepbx.service << EOF
[Unit]
Description=Freepbx
After=mariadb.service
 
[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/usr/sbin/fwconsole start -q
ExecStop=/usr/sbin/fwconsole stop -q
 
[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable freepbx

echo "
 +-+-+-+ +-+-+-+-+ +-+-+-+-+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+
 |Y|o|u| |h|a|v|e| |s|u|c|c|e|s|s|f|u|l|l|y| |i|n|s|t|a|l|l|e|d| |F|r|e|e|P|B|X|
 +-+-+-+ +-+-+-+-+ +-+-+-+-+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+
"

rm -rf /tmp/freepbx_debian_install.sh
