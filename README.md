
This is a FreePBX 17 installation script.

### How to execute the script

Steps -
1) ssh to the debian system as 'root' user
2) wget https://gitlab.com/UPNF/pbxinstall/-/raw/main/freepbx_debian_install.sh -O /tmp/freepbx_debian_install.sh && chmod +x /tmp/freepbx_debian_install.sh && bash /tmp/freepbx_debian_install.sh 

The script will install the necessary dependencies for FreePBX, followed by the FreePBX software itself. 

The installation duration may vary depending on your internet bandwidth and system capacity.
